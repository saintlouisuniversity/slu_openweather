<?php

class SluOpenweatherDataObject extends KGODataObject
{
    const CITY_ATTRIBUTE = 'sluow:city';
    const SUNRISE_ATTRIBUTE = 'sluow:sunrise';
    const SUNSET_ATTRIBUTE = 'sluow:sunset';
    const TEMP_ATTRIBUTE = 'sluow:temp';
    const WSP_ATTRIBUTE = 'sluow:windspeed';
    const WDIR_ATTRIBUTE = 'sluow:winddir';
    const SKY_ATTRIBUTE = 'sluow:sky';
    const HUM_ATTRIBUTE = 'sluow:humidity';
    const PRESS_ATTRIBUTE = 'sluow:pressure';
    const UPD_ATTRIBUTE = 'sluow:update';

    
    public function getCity() {
        return $this->getAttribute(self::CITY_ATTRIBUTE);
    }

    public function getSunrise() {

		$dateArr = explode("T",$this->getAttribute(self::SUNRISE_ATTRIBUTE));
		$timeArr = explode(":",$dateArr[1]);
		$h = $timeArr[0];
		$m = $timeArr[1];
		$s = $timeArr[2];
		if($h<=5)
		{
			$h=$h+7;
		}
		else
		{
			$h=$h-5;
		}
		$ap ="am";
		if($h>11)
        {
			$h = $h-12;
		}
		$t = $h.":".$m." ".$ap;
		return $t;

    }

    public function getSunset() {
		$dateArr = explode("T",$this->getAttribute(self::SUNSET_ATTRIBUTE));
		$timeArr = explode(":",$dateArr[1]);
		$h = $timeArr[0];
		$m = $timeArr[1];
		$s = $timeArr[2];
		if($h<=5)
		{
			$h=$h+7;
		}
		else
		{
			$h=$h-5;
		}
		$ap ="pm";
		if($h>11)
        {
			$h = $h-12;
		}
		$t = $h.":".$m." ".$ap;
		return $t;
    }

    public function getTemp() {
        return round($this->getAttribute(self::TEMP_ATTRIBUTE));
    }

    public function getWSP() {
        return round($this->getAttribute(self::WSP_ATTRIBUTE));
    }
    public function getWDIR() {
        return $this->getAttribute(self::WDIR_ATTRIBUTE);
    }
    public function getPress() {
        $press =  $this->getAttribute(self::PRESS_ATTRIBUTE)/33.8653075;
        return round($press,2);
    }
    public function getHum() {
        return $this->getAttribute(self::HUM_ATTRIBUTE);
    }
    public function getSky() {
        return $this->getAttribute(self::SKY_ATTRIBUTE);
    }
    public function getUpd() {
		$dateArr = explode("T",$this->getAttribute(self::UPD_ATTRIBUTE));
		$timeArr = explode(":",$dateArr[1]);
		$h = $timeArr[0];
		$m = $timeArr[1];
		$s = $timeArr[2];
		if($h<=5)
		{
			$h=$h+7;
		}
		else
		{
			$h=$h-5;
		}
		$t = $h.":".$m.":".$s;
		return $t;
    }

}
