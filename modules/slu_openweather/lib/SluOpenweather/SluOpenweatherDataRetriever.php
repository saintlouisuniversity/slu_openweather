<?php

/*
 * Copyright © 2010 - 2014 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

class SluOpenweatherDataRetriever extends KGOURLDataRetriever {
    public static $sluopenweatherURLTemplate = "http://api.openweathermap.org/data/2.5/weather?zip=%s,us&mode=xml&units=imperial&appid=%s&lang=en";
    protected function init($args) {
        parent::init($args);
        $this->setCacheLifeTime(60);
        $this->setBaseURL(sprintf(self::$sluopenweatherURLTemplate, $args['zipcode'], $args['appKey']));
    }
}
