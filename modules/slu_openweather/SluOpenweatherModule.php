<?php


class SluOpenweatherModule extends KGOModule {

    /*
     *  The initializeForPageConfigObjects_ methods below don't need to do much, they simply check if a feed has been configured
     *  The $objects configured in the page objdefs will take control from here
     */

    protected function initializeForPage_index(KGOUIPage $page) {
        $html = new KGOUIHtml();
         $html->setField('html', '<p>Current Weather retrieved from Openweathermap.org</p>');
        $page->appendToRegionContents('content', $html);
    }
    protected function initializeForPageConfigObjects_index(KGOUIPage $page, $objects) {
        if (!($feed = $this->getFeed())) {
            $this->setPageError($page, "Unable to load events");
            return;
        }
    }

    public function getOpenweather() {
        if($feed = $this->getFeed()) {
            $items = $feed->getRetriever()->getData($response);
            return current($items);
          
       }
    }


}